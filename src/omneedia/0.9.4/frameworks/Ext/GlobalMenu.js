App.__NOTIFY__ = document.createElement("div");
App.__NOTIFY__.hide = true;

document.getElementsByTagName("body")[0].appendChild(App.__NOTIFY__);
Ext.define("omneedia.webapp.GlobalMenu", {
  extend: "Ext.Panel",
  xtype: "Menu",
  border: false,
  constructor: function (cnfg) {
    var _p = this;
    this.callParent(arguments);
    this.initConfig(cnfg);
    var menu = [];

    menu[0] = {
      iconCls: "qxIcon",
      text: "<b>" + window.document.title + "</b>",
      menu: {
        items: [
          {
            text: '<span class="ABOUT"></span>',
            handler: function () {
              Ext.create("Ext.Window", {
                title: '<span class="ABOUT"></span>',
                width: 500,
                modal: true,
                closable: true,
                resizable: false,
                height: 331,
                layout: "vbox",
                bodyStyle: "background-color:white",
                border: false,
                buttonAlign: "center",
                listeners: {
                  show: function () {
                    /*
                                    var Authors="";
                                    for (var i=0;i<Settings.AUTHORS.length;i++)
                                    {
                                    	Authors+='<br><a href="mailto:'+Settings.AUTHORS[i].email+'">'+Settings.AUTHORS[i].name+'</a><br><small>'+Settings.AUTHORS[i].company+'</small>';
                                    };
                                    //this.items.items[2].update('<div class=AboutBoxTitle>'+window.document.title+'</div><div class=AboutBoxVersion>version '+Settings.VERSION+'.'+Settings.BUILD+'<br><br>Auteurs :'+Authors+'</div>');
                                    */
                  },
                },
                bbar: [
                  {
                    text: "Credits",
                    hidden: true,
                    handler: function () {
                      Ext.create("Ext.Window", {
                        title: "Credits",
                        width: 400,
                        height: 500,
                        modal: true,
                        closable: true,
                        resizable: false,
                        layout: "vbox",
                        bodyStyle: "background-color:white",
                        border: false,
                        bbar: [
                          "->",
                          {
                            text: '<span class="CLOSE"></span>',
                            handler: function (w) {
                              w.up().up().close();
                            },
                          },
                        ],
                        items: [],
                      })
                        .show()
                        .center();
                    },
                  },
                  {
                    text: "Licence",
                    hidden: true,
                    handler: function () {
                      Ext.create("Ext.Window", {
                        title: "License",
                        width: 640,
                        height: 550,
                        modal: true,
                        closable: true,
                        resizable: true,
                        layout: "vbox",
                        bodyStyle: "background-color:white",
                        border: false,
                        bbar: [
                          "->",
                          {
                            text: '<span class="CLOSE"></span>',
                            handler: function (w) {
                              w.up().up().close();
                            },
                          },
                        ],
                        items: [],
                      })
                        .show()
                        .center();
                    },
                  },
                  "->",
                  {
                    text: '<span class="CLOSE"></span>',
                    handler: function (w) {
                      w.up().up().close();
                    },
                  },
                ],
                items: [
                  {
                    height: 10,
                    border: false,
                  },
                  {
                    layout: "hbox",
                    border: false,
                    items: [
                      {
                        xtype: "panel",
                        border: false,
                        baseCls: "qxAboutLogo",
                        width: 150,
                        height: 150,
                      },
                      {
                        xtype: "panel",
                        flex: 1,
                        height: 150,
                        width: 500,
                        border: false,
                        html:
                          "<div class=AboutBoxTitle>" +
                          window.document.title +
                          "<div class=AboutBoxDescription>" +
                          Settings.DESCRIPTION +
                          "</div><div class=AboutBoxVersion>version " +
                          Settings.VERSION +
                          "." +
                          Settings.BUILD +
                          "</div><div class=AboutBoxCopyright>" +
                          Settings.COPYRIGHT +
                          "</div>",
                      },
                    ],
                  },
                  {
                    layout: "hbox",
                    width: "100%",
                    border: false,
                    items: [
                      {
                        border: false,
                        html:
                          '<div class=AboutBoxLicence>EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS PROVIDE THE SOFTWARE "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE SOFTWARE IS WITH YOU. SHOULD THE SOFTWARE PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION. IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING WILL ANY COPYRIGHT HOLDER, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OR INABILITY TO USE THE SOFTWARE (INCLUDING BUT NOT LIMITED TO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD PARTIES OR A FAILURE OF THE SOFTWARE TO OPERATE WITH ANY OTHER PROGRAMS), EVEN IF SUCH HOLDER HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.</div>',
                        flex: 1,
                      },
                    ],
                  },
                ],
              })
                .show()
                .center();
            },
          },
          {
            iconCls: "mnu_preferences",
            id: "mnu_preferences",
            text: '<span class="PREFS"></span>',
            handler: function () {
              if (!App.prefs) return;
              Ext.create("Ext.Window", {
                title: '<span class="PREFS"></span>',
                width: App.prefs.width,
                modal: true,
                closable: true,
                resizable: false,
                height: App.prefs.height,
                layout: "vbox",
                bodyStyle: "background-color:white",
                border: false,
                buttonAlign: "center",
                layout: "vbox",
                items: App.prefs.items,
                listeners: {
                  show: App.prefs.onShow,
                },
              })
                .show()
                .center();
            },
          },
        ],
      },
    };
    for (var i = 0; i < this.menu.length; i++) {
      menu[i + 1] = this.menu[i];
    }
    menu[i + 1] = "->";
    menu[i + 2] = {
      id: "GlobalMenuUser",
      menu: {
        items: [
          {
            text: '<span class="LOGOUT"></span>',
            handler: function () {
              Auth.logout();
            },
          },
        ],
      },
    };
    menu[i + 3] = {
      id: "GlobalMenuDateTime",
    };
    menu[i + 4] = {
      id: "GlobalMenuLang",
      menu: {
        items: [],
      },
    };
    menu[i + 5] = {
      hidden: true,
      iconCls: "QxNotificationBar",
      handler: function () {
        App.__NOTIFY__.center = document.createElement("iframe");
        App.__NOTIFY__.center.style.width = "100%";
        App.__NOTIFY__.center.style.height = "100%";
        App.__NOTIFY__.center.style.position = "absolute";
        App.__NOTIFY__.center.style.left = "0px";
        App.__NOTIFY__.center.style.top = "0px";
        App.__NOTIFY__.center.style.border = "0px solid black";
        App.__NOTIFY__.center.src = "/notification-center";
        App.__NOTIFY__.center.id = App.uuid();
        App.__NOTIFY__.appendChild(App.__NOTIFY__.center);
        App.__NOTIFY__.center.onload = function () {
          var dom = App.__NOTIFY__.center.contentWindow.document;
          var today = new Date();
          dom.querySelector(".date").innerHTML =
            today.toString("dddd") +
            "<br>" +
            today.toString("dd") +
            " " +
            today.toString("MMMM");
        };
        if (App.__NOTIFY__.hide) {
          App.__NOTIFY__.hide = false;
          App.__NOTIFY__.classList.add("OASlidingPanel");
          App.__NOTIFY__.classList.add("animated");
          App.__NOTIFY__.classList.remove("slideOutRight");
          App.__NOTIFY__.classList.add("slideInRight");
        } else {
          App.__NOTIFY__.hide = true;
          App.__NOTIFY__.classList.remove("slideInRight");
          App.__NOTIFY__.classList.add("slideOutRight");
        }
      },
    };
    var tbar = new Ext.Toolbar({
      items: menu,
    });
    this.addDocked(tbar);
    if (!App.prefs) Ext.getCmp("mnu_preferences").hide();
    window.setInterval(function () {
      Ext.getCmp("GlobalMenuDateTime").setText(moment().format("llll"));
      if (Ext.getCmp("GlobalMenuUser")) {
        var o = Auth.User;
        if (o) {
          try {
            Ext.getCmp("GlobalMenuUser").show();
            var text = Ext.getCmp("GlobalMenuUser").getText();
            if (text != o.mail)
              Ext.getCmp("GlobalMenuUser").setText(o.mail.split("@")[0]);
          } catch (e) {
            Ext.getCmp("GlobalMenuUser").hide();
          }
        }
      }
    }, 1000);

    if (Ext.getCmp("GlobalMenuUser")) {
      Ext.getCmp("GlobalMenuUser").setText("...");

      for (var i = 0; i < Settings.LANGS.length; i++) {
        if (i == 0) var default_text = Settings.LANGS[i].toUpperCase();
        Ext.getCmp("GlobalMenuLang").menu.add({
          text: Settings.LANGS[i].toUpperCase(),
          handler: function (x) {
            moment.locale(x.text.toLowerCase());
            window.localStorage.LANG = x.text.toLowerCase();
            document.body.lang = window.localStorage.LANG;
            Ext.getCmp("GlobalMenuLang").setText(
              document.body.lang.toUpperCase()
            );
            /*
             *
             * Fingerprint
             *
             */

            Ext.Ajax.setDefaultHeaders({
              //"accept-encoding": true,
              i18n: window.localStorage.LANG,
              Authorization:
                "Bearer " +
                window.localStorage.getItem("socketcluster.authToken"),
            });
            Ext.override(Ext.data.proxy.Ajax, {
              headers: {
                i18n: window.localStorage.LANG,
                //"accept-encoding": true,
                Authorization:
                  "Bearer " +
                  window.localStorage.getItem("socketcluster.authToken"),
              },
            });
          },
        });

        if (window.localStorage.LANG) {
          Ext.getCmp("GlobalMenuLang").setText(
            window.localStorage.LANG.toUpperCase()
          );
          if (moment) moment.locale(window.localStorage.LANG.toLowerCase());
        } else {
          Ext.getCmp("GlobalMenuLang").setText(default_text.toUpperCase());
          window.localStorage.LANG = default_text.toLowerCase();
          if (moment) moment.locale(window.localStorage.LANG.toLowerCase());
        }
        /*
         *
         * Fingerprint
         *
         */

        Ext.Ajax.setDefaultHeaders({
          //"accept-encoding": true,
          i18n: window.localStorage.LANG,
          Authorization:
            "Bearer " + window.localStorage.getItem("socketcluster.authToken"),
        });
        Ext.override(Ext.data.proxy.Ajax, {
          headers: {
            i18n: window.localStorage.LANG,
            //"accept-encoding": true,
            Authorization:
              "Bearer " +
              window.localStorage.getItem("socketcluster.authToken"),
          },
        });
      }
    }
  },
});
